using Avalonia.Controls;
using Avalonia.Interactivity;

namespace App.Views;

public abstract class UserControlBase : UserControl
{ 
    public abstract void FocusToBtnSubmit(object? sender, RoutedEventArgs e);
}