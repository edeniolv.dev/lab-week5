using Avalonia.Interactivity;

namespace App.Views;

public partial class NotificationView : UserControlBase
{
    public NotificationView()
    {
        InitializeComponent();
    }

    public override void FocusToBtnSubmit(object? sender, RoutedEventArgs e)
    {
        BtnYes.Focus();
        BtnYes.IsDefault = true;
    }
}