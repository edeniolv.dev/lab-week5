using Avalonia.Interactivity;

namespace App.Views;

public partial class SearchView : UserControlBase
{
    public SearchView()
    {
        InitializeComponent();
    }

    public override void FocusToBtnSubmit(object? sender, RoutedEventArgs e)
    {
        FldEmail.Focus();
        BtnSubmit.IsDefault = true;
    }
}