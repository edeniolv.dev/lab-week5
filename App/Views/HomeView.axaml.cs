using Avalonia.Controls;

namespace App.Views;

public partial class HomeView : Window
{
    public HomeView()
    {
        InitializeComponent();
        SizeToContent = SizeToContent.Manual;
        CanResize = false;
        Resized += OnResizeManual;
    }
    
    private void OnResizeManual(object? sender, WindowResizedEventArgs e)
    {
        Width = 1280;
        Height = 720;
    }
}
