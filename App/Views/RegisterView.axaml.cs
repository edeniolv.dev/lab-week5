using Avalonia.Interactivity;

namespace App.Views;

public partial class RegisterView : UserControlBase
{
    public RegisterView()
    {
        InitializeComponent();
    }

    public override void FocusToBtnSubmit(object? sender, RoutedEventArgs e)
    {
        BtnSubmit.Focus();
        BtnSubmit.IsDefault = true;
    }

    public void OnClickBtnSubmit(object? sender, RoutedEventArgs e)
    {
        FldFirstName.Focus();
    }
}