using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using App.Models.Entity;

namespace App.Models.Repository;

public class UserRepository
{
    private ObservableCollection<User> Users { get; } = new();
    public ObservableCollection<User>? FilteredUsers { get; set; }
    private IEnumerable<User>? QueryFilter { get; set; }
    private IOrderedEnumerable<User>? QueryOrder { get; set; }
    
    public void Add(User getUser)
    {
        Users.Add(getUser);
    }

    public ObservableCollection<User> FindAll()
    {
        return Users;
    }

    public ObservableCollection<User> FindByEmail(string email)
    {
        QueryFilter = FilteredUsers switch
        {
            null => Users.Where(user => user.Email!.Contains(email)),
            _ => FilteredUsers.Where(user => user.Email!.Contains(email))
        };

        return FilteredUsers = 
            new ObservableCollection<User>(QueryFilter);
    }
    
    public ObservableCollection<User> FindByAscendingOrder()
    {
        QueryOrder = FilteredUsers switch
        {
            null => Users.OrderBy(user => user.FirstName),
            _ => FilteredUsers?.OrderBy(user => user.FirstName)
        };

        return FilteredUsers = 
            new ObservableCollection<User>(QueryOrder!);
    }

    public ObservableCollection<User> FindByDescendingOrder()
    {
        QueryOrder = FilteredUsers switch
        {
            null => Users.OrderByDescending(user => user.FirstName),
            _ => FilteredUsers?.OrderByDescending(user => user.FirstName)
        };

        return FilteredUsers = 
            new ObservableCollection<User>(QueryOrder!);
    }
}
