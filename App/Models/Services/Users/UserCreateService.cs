using App.Models.Entity;
using App.Models.Repository;

namespace App.Models.Services.Users;

public class UserCreateService : IUserListener
{
    public void ReceiveUserNotification(object? sender, UserRepository repository)
    {
        if (sender is User user)
        {
            repository.Add(user);
        }
    }
}
