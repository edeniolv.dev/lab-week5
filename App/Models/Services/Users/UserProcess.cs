using System.Collections.Generic;
using App.Models.Repository;

namespace App.Models.Services.Users;

public class UserProcess : ISubject<IUserListener>
{
    public static UserProcess Instance => _instance ??= new UserProcess();
    private List<IUserListener> Observers { get; } = new();
    private UserRepository Repository { get; } = new();
    
    private static UserProcess? _instance;
    
    private UserProcess() {}

    public void Attach(IUserListener userListener)
    {
        Observers.Add(userListener);
    }

    public void Detach(IUserListener userListener)
    {
        Observers.Remove(userListener);
    }

    public void Notify(object? value = null)
    {
        foreach (IUserListener observer in Observers)
        {
            observer.ReceiveUserNotification(value, Repository);
        }
    }
}

