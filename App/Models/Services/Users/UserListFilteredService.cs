using App.Models.Entity;
using App.Models.Repository;

namespace App.Models.Services.Users;

public class UserListFilteredService : IUserListener
{
    public event IUserListener.ListUserHandler? GetListUsers;

    public void ReceiveUserNotification(object? sender, UserRepository repository)
    {
        if (sender is string value)
        {
            GetListUsers?.Invoke(repository.FindByEmail(value));
        }

        if (sender is Order order)
        {
            switch (order)
            {
                case Order.Ascending:
                    GetListUsers?.Invoke(repository.FindByAscendingOrder());
                    break;
                case Order.Descending:
                    GetListUsers?.Invoke(repository.FindByDescendingOrder());
                    break;
            }
        }
    }
}
