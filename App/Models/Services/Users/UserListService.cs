using App.Models.Repository;

namespace App.Models.Services.Users;

public class UserListService : IUserListener
{
    public event IUserListener.ListUserHandler? GetListUsers;
    
    public void ReceiveUserNotification(object? sender, UserRepository repository)
    {
        if (repository.FilteredUsers != null)
        {
            repository.FilteredUsers = null;
        }
        GetListUsers?.Invoke(repository.FindAll());
    }
}
