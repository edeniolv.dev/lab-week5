namespace App.Models.Services.Notifications;

public class InfoNotificationService : INotificationListener
{
    public event INotificationListener.NotificationHandler? SetMessageNotification;
    
    public void ReceiveMessageNotification(object? sender)
    {
        if (sender is string message)
        {
            SetMessageNotification?.Invoke(message);
        }
    }
}
