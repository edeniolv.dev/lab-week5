using System.Collections.Generic;

namespace App.Models.Services.Notifications;

public class NotificationProcess : ISubject<INotificationListener>
{
    public static NotificationProcess Instance => _instance ??= new NotificationProcess();
    private List<INotificationListener> Observers { get; } = new();
    
    private static NotificationProcess? _instance;
    
    private NotificationProcess() {}
    
    public void Attach(INotificationListener notificationListener)
    {
        Observers.Add(notificationListener);
    }

    public void Detach(INotificationListener notificationListener)
    {
        Observers.Remove(notificationListener);
    }

    public void Notify(object? value)
    {
        foreach (INotificationListener observer in Observers)
        {
            observer.ReceiveMessageNotification(value);
        }
    }
}
