using System.Collections.ObjectModel;
using App.Models.Entity;
using App.Models.Repository;

namespace App.Models;

public interface IUserListener
{ 
    public delegate void ListUserHandler(ObservableCollection<User> users);
    void ReceiveUserNotification(object? sender, UserRepository repository);
}
