namespace App.Models;

public interface ISubject<in T>
{
    public void Attach(T userListener);
    public void Detach(T userListener);
    public void Notify(object? value);
}
