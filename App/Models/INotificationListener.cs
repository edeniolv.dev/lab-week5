namespace App.Models;

public interface INotificationListener
{
    public delegate void NotificationHandler(string users);
    void ReceiveMessageNotification(object? sender);
}
