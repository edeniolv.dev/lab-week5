namespace App.Models.Entity;

public enum Order
{
    Ascending,
    Descending
}
