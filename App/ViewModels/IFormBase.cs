namespace App.ViewModels;

public interface IFormBase
{
    void OnSubmitClick();
    void SetMessage(string message);
    bool IsEmptyOrNull<T>(T? property);
    void MessageDurationTime(int ms);
}