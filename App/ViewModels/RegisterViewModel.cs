using System.Reactive;
using App.Models.Entity;
using ReactiveUI;
using System.Timers;
using App.Models;
using App.Models.Services.Notifications;
using App.Models.Services.Users;

namespace App.ViewModels;

public class RegisterViewModel : ViewModelBase, IFormBase
{
    public ReactiveCommand<Unit, Unit>? SubmitCommand { get; private set; }
    private UserCreateService UserCreateService { get; } = new();
    private InfoNotificationService InfoNotificationService { get; } = new();
    private ErrorNotificationService ErrorNotificationService { get; } = new();
    private User? User { get; set; }
    public string? FirstName
    {
        get => _firstName;
        set => this.RaiseAndSetIfChanged(ref _firstName, value);
    }
    public string? LastName
    {
        get => _lastName;
        set => this.RaiseAndSetIfChanged(ref _lastName, value);
    }
    public string? Email
    {
        get => _email;
        set => this.RaiseAndSetIfChanged(ref _email, value);
    }
    public string? TextMessage
    {
        get => _textMessage;
        private set => this.RaiseAndSetIfChanged(ref _textMessage, value);
    }
    
    private string? _firstName;
    private string? _lastName;
    private string? _email;
    private string? _textMessage;
    
    public RegisterViewModel()
    {
        BuildCommands();
        BuildEvents();
        BuildObservers();
    }

    private void BuildCommands()
    {
        SubmitCommand = ReactiveCommand.Create(OnSubmitClick);
    }

    private void BuildObservers()
    {
        UserProcess.Instance.Attach(UserCreateService);
    }

    private void BuildEvents()
    {
        InfoNotificationService.SetMessageNotification += SetMessage;
        ErrorNotificationService.SetMessageNotification += SetMessage;
    }
    
    private void SetObserverAndNotifyMessage(
        INotificationListener observer, 
        string? message
    )
    {
        NotificationProcess.Instance.Attach(observer);
        NotificationProcess.Instance.Notify(message);
        NotificationProcess.Instance.Detach(observer);
    }
    
    public void OnSubmitClick()
    {
        if (IsInvalidFields())
        {
            SetObserverAndNotifyMessage(
                ErrorNotificationService, 
                "Empty fields not allowed..."
            );
            return;
        }
        
        User = new User(FirstName, LastName, Email);
        
        UserProcess.Instance.Notify(User);
        
        SetObserverAndNotifyMessage(
            InfoNotificationService, 
            "Record created successfully!"
        );
        CleanAllFields();
    }

    public void SetMessage(string message)
    {
        TextMessage = message;
        MessageDurationTime(2500);
    }

    private void CleanAllFields()
    {
        FirstName = string.Empty;
        LastName = string.Empty;
        Email = string.Empty;
    }
    
    public bool IsEmptyOrNull<T>(T? property)
    {
        if (property is string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return true;
            }
        }
        return false;
    }

    private bool IsInvalidFields()
    {
        return 
            IsEmptyOrNull(FirstName) || 
            IsEmptyOrNull(LastName) || 
            IsEmptyOrNull(Email);
    }
    
    public void MessageDurationTime(int ms)
    {
        Timer timer = new Timer(ms);
        timer.Elapsed += (_, _) => TextMessage = string.Empty;
        timer.AutoReset = false;
        timer.Start();
    }
}
