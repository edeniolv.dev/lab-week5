﻿using System.Reactive;
using App.Views;
using Avalonia.Controls;
using ReactiveUI;

namespace App.ViewModels;

public class HomeViewModel : ViewModelBase
{
    public ReactiveCommand<Unit, Unit>? RegisterCommand { get; }
    public ReactiveCommand<Unit, Unit>? SearchCommand { get; }
    private RegisterView RegisterView { get; } = new();
    public RegisterViewModel RegisterViewModel { get; } = new();
    private SearchView SearchView { get; } = new();
    public SearchViewModel SearchViewModel { get; } = new();
    private NotificationView NotificationView { get; } = new();
    public NotificationViewModel NotificationViewModel { get; } = new();
    public Control? CenterContent
    {
        get => _centerContent;
        private set => this.RaiseAndSetIfChanged(
            ref _centerContent, 
            value
        );
    }
    public bool IsBtnRegisterEnable
    {
        get => _isBtnRegisterEnable;
        private set => this.RaiseAndSetIfChanged(
            ref _isBtnRegisterEnable, 
            value
        );
    }
    public bool IsBtnSearchEnable
    {
        get => _isBtnSearchEnable;
        private set => this.RaiseAndSetIfChanged(
            ref _isBtnSearchEnable, 
            value
        );
    }
    
    private Control? _centerContent;
    private bool _isBtnRegisterEnable = true;
    private bool _isBtnSearchEnable = true;

    public HomeViewModel()
    {
        IsBtnRegisterEnable = false;
        CenterContent = RegisterView;
        
        RegisterCommand = ReactiveCommand.Create(OnClickRegister);
        SearchCommand = ReactiveCommand.Create(OnClickSearch);

        NotificationViewModel.IsConfirmed += OnYesPressed;
        NotificationViewModel.IsCanceled += OnNoPressed;
    }

    private void OnClickRegister()
    {
        IsBtnRegisterEnable = false;

        if (CenterContent != null)
        {
            ClearSearchFields();
        }

        SetStateCenterContent(
            RegisterView, 
            true, 
            false
        );
    }

    private void OnClickSearch()
    {
        IsBtnSearchEnable = false;
        
        if (CenterContent != null)
        {
            CenterContent = NotificationView;
            return;
        }
        CenterContent = SearchView;
    }
    
    private void OnYesPressed()
    {
        SetStateCenterContent(
            SearchView, 
            false, 
            true
        );
        ClearRegisterFields();
    }
    
    private void OnNoPressed()
    {
        SetStateCenterContent(
            RegisterView, 
            true, 
            false
        );
    }
    
    private void SetStateCenterContent(
        UserControlBase userControl, 
        bool isSearchEnable, 
        bool isRegisterEnable
    )
    {
        CenterContent = userControl;
        IsBtnSearchEnable = isSearchEnable;
        IsBtnRegisterEnable = isRegisterEnable;
    }

    private void ClearRegisterFields()
    {
        RegisterViewModel.FirstName = string.Empty;
        RegisterViewModel.LastName = string.Empty;
        RegisterViewModel.Email = string.Empty;
    }

    private void ClearSearchFields()
    {
        SearchViewModel.Email = string.Empty;
    }
}
