using System.Reactive;
using ReactiveUI;

namespace App.ViewModels;

public class NotificationViewModel : ViewModelBase
{
    public delegate void NotificationHandler();
    public event NotificationHandler? IsConfirmed;
    public event NotificationHandler? IsCanceled;
    public ReactiveCommand<Unit, Unit> BtnYesPressed { get; }
    public ReactiveCommand<Unit, Unit> BtnNoPressed { get; }
    
    public NotificationViewModel()
    {
        BtnYesPressed = ReactiveCommand.Create(OnPressedYesAction);
        BtnNoPressed = ReactiveCommand.Create(OnPressedNoAction);
    }

    private void OnPressedYesAction()
    {
        IsConfirmed?.Invoke();
    }
    
    private void OnPressedNoAction()
    {
        IsCanceled?.Invoke();
    }
}
