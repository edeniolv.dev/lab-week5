using System.Collections.ObjectModel;
using System.Reactive;
using App.Models;
using ReactiveUI;
using App.Models.Entity;
using App.Models.Services.Notifications;
using App.Models.Services.Users;
using Timer = System.Timers.Timer;

namespace App.ViewModels;

public class SearchViewModel : ViewModelBase, IFormBase
{
    public ReactiveCommand<Unit, Unit>? SubmitCommand { get; private set; }
    public ReactiveCommand<Unit, Unit>? ResetCommand { get; private set; }
    public ReactiveCommand<Unit, Unit>? AscendingCommand { get; private set; }
    public ReactiveCommand<Unit, Unit>? DescendingCommand { get; private set; }
    private UserListService UserListService { get; } = new();
    private UserListFilteredService UserListFilteredService { get; } = new();
    private InfoNotificationService InfoNotificationService { get; } = new();
    private ErrorNotificationService ErrorNotificationService { get; } = new();
    public ObservableCollection<User>? UsersFiltered
    {
        get => _users;
        set => this.RaiseAndSetIfChanged(ref _users, value);
    }
    public string? Email
    {
        get => _email;
        set => this.RaiseAndSetIfChanged(ref _email, value);
    }
    public string? TotalUsers
    {
        get => _totalUsers;
        set => this.RaiseAndSetIfChanged(ref _totalUsers, value);
    }
    public string? TotalFilteredUsers
    {
        get => _totalFilteredUsers;
        set => this.RaiseAndSetIfChanged(ref _totalFilteredUsers, value);
    }
    public string? TextMessage
    {
        get => _textMessage;
        private set => this.RaiseAndSetIfChanged(ref _textMessage, value);
    }
    public bool? IsAscendingChecked
    {
        get => _isAscendingChecked;
        private set => this.RaiseAndSetIfChanged(ref _isAscendingChecked, value);
    }
    public bool? IsDescendingChecked
    {
        get => _isDescendingChecked;
        private set => this.RaiseAndSetIfChanged(ref _isDescendingChecked, value);
    }
    public bool? IsEnableAscendingCheck
    {
        get => _isEnableAscendingCheck;
        private set => this.RaiseAndSetIfChanged(ref _isEnableAscendingCheck, value);
    }
    public bool? IsEnableDescendingCheck
    {
        get => _isEnableDescendingCheck;
        private set => this.RaiseAndSetIfChanged(ref _isEnableDescendingCheck, value);
    }
    
    private string? _email;
    private string? _totalUsers;
    private string? _totalFilteredUsers;
    private string? _textMessage;
    private bool? _isAscendingChecked;
    private bool? _isDescendingChecked;
    private bool? _isEnableAscendingCheck;
    private bool? _isEnableDescendingCheck;
    private ObservableCollection<User>? _users;
    
    public SearchViewModel()
    {
        BuildCommands();
        BuildEvents();
        BuildObservers();
        
        TotalUsers = GetTotalUsers(UsersFiltered);
        TotalFilteredUsers = GetTotalUsers(UsersFiltered);
    }

    private void BuildCommands()
    {
        SubmitCommand = ReactiveCommand.Create(OnSubmitClick);
        ResetCommand = ReactiveCommand.Create(OnResetClick);
        AscendingCommand = ReactiveCommand.Create(OnAscendingOrderClick);
        DescendingCommand = ReactiveCommand.Create(OnDescendingOrderClick);
    }

    private void BuildEvents()
    {
        UserListService.GetListUsers += SetAllUsers;
        UserListFilteredService.GetListUsers += SetFilteredUsers;
        
        InfoNotificationService.SetMessageNotification += SetMessage;
        ErrorNotificationService.SetMessageNotification += SetMessage;
    }

    private void BuildObservers()
    {
        UserProcess.Instance.Attach(UserListService);
        UserProcess.Instance.Attach(UserListFilteredService);
    }
    
    public void OnSubmitClick()
    {
        OnUncheckedRadioButton();
        SetEnableRadioButton(true);

        if (ValidateIfIsEmpty())
        {
            return;
        }

        if (ValidateIfNotFound())
        {
            return;
        }

        if (ValidateIfIsValid())
        {
            CleanAllFields();
            SetObserverAndNotifyMessage(
                InfoNotificationService,
                $"Emails found by filter: {Email}"
            );
        }
    }

    private bool ValidateIfIsEmpty()
    {
        if (IsEmptyOrNull(Email))
        {
            IsLessThan();
            CleanAllFields();
            SetObserverAndNotifyMessage(
                ErrorNotificationService,
                "Empty fields not allowed..."
            );
            return true;
        }
        return false;
    }

    private bool ValidateIfNotFound()
    {
        if (IsEmptyOrNull(UsersFiltered))
        {
            IsLessThan();
            CleanAllFields();
            SetObserverAndNotifyMessage(
                ErrorNotificationService,
                $"Value '{Email}' not found or list is empty..."
            );
            return true;
        }
        return false;
    }

    private bool ValidateIfIsValid()
    {
        if (UsersFiltered != null)
        {
            DetachObserverAndNotifyUser(Email);
            TotalFilteredUsers = GetTotalUsers(UsersFiltered);
            IsLessThan();

            return true;
        }
        return false;
    }
    
    private void IsLessThan()
    {
        if (UsersFiltered?.Count <= 1)
        {
            SetEnableRadioButton(false);
        }
    }
    
    private void OnResetClick()
    {
        OnUncheckedRadioButton();
        SetEnableRadioButton(true);
        AttachObserverAndNotify();
        
        Email = string.Empty;
        TotalFilteredUsers = GetTotalUsers(UsersFiltered);
        
        SetObserverAndNotifyMessage(
            InfoNotificationService,
            "Resetting filters..."
        );
    }
    
    private void OnAscendingOrderClick()
    {
        if (UsersFiltered != null)
        {
            DetachObserverAndNotifyUser(Order.Ascending);
            TotalFilteredUsers = GetTotalUsers(UsersFiltered);
        }
        
        SetObserverAndNotifyMessage(
            InfoNotificationService,
            "Order set by Ascending..."
        );
    }
    
    private void OnDescendingOrderClick()
    {
        if (UsersFiltered != null)
        {
            DetachObserverAndNotifyUser(Order.Descending);
            TotalFilteredUsers = GetTotalUsers(UsersFiltered);
        }
        
        SetObserverAndNotifyMessage(
            InfoNotificationService,
            "Order set by Descending..."
        );
    }
    
    private void SetAllUsers(ObservableCollection<User> users)
    {
        UsersFiltered = users;
        TotalUsers = GetTotalUsers(UsersFiltered);
        TotalFilteredUsers = GetTotalUsers(UsersFiltered);
    }

    private void SetFilteredUsers(ObservableCollection<User> users)
    {
        UsersFiltered = users;
        TotalFilteredUsers = GetTotalUsers(UsersFiltered);
    }
    
    private string GetTotalUsers(ObservableCollection<User>? users)
    {
        if (users?.Count > 0)
        {
            return users.Count.ToString();
        }
        return "0";
    }
    
    private void SetObserverAndNotifyMessage(INotificationListener observer, string? message)
    {
        NotificationProcess.Instance.Attach(observer);
        NotificationProcess.Instance.Notify(message);
        NotificationProcess.Instance.Detach(observer);
    }
    
    private void DetachObserverAndNotifyUser(object? state)
    {
        UserProcess.Instance.Detach(UserListService);
        UserProcess.Instance.Notify(state);
    }
    
    private void AttachObserverAndNotify(string? state = "")
    {
        UserProcess.Instance.Attach(UserListService);
        UserProcess.Instance.Notify(state);
    }
    
    private void SetEnableRadioButton(bool condition)
    {
        IsEnableAscendingCheck = condition;
        IsEnableDescendingCheck = condition;
    }
    
    private void OnUncheckedRadioButton()
    {
        IsAscendingChecked = false;
        IsDescendingChecked = false;
    }

    private void CleanAllFields()
    {
        Email = string.Empty;
    }
    
    public bool IsEmptyOrNull<T>(T? property)
    {
        if (property == null)
        {
            return true;
        }
        
        if (property is string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return true;
            }
        }
        
        return false;
    }

    public void SetMessage(string message)
    {
        TextMessage = message;
        MessageDurationTime(2500);
    }
    
    public void MessageDurationTime(int ms)
    {
        Timer timer = new Timer(ms);
        timer.Elapsed += (_, _) => TextMessage = string.Empty;
        timer.AutoReset = false;
        timer.Start();
    }
}
